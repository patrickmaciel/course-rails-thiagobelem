require 'dragonfly'

# Configure
Dragonfly.app.configure do
  plugin :imagemagick

  secret "544b441cd1c30a12932da20ecfa89cfb19ff5d9c8a86ce0e0eba2ca639ae75e8"

  url_format "/media/:job/:name"

  if Rails.env.production?
    # S3
  else
    datastore :file,
      root_path: Rails.root.join('public/system/dragonfly', Rails.env),
      server_root: Rails.root.join('public')
  end
end

# Logger
Dragonfly.logger = Rails.logger

# Mount as middleware
Rails.application.middleware.use Dragonfly::Middleware

# Add model functionality
if defined?(ActiveRecord::Base)
  ActiveRecord::Base.extend Dragonfly::Model
  ActiveRecord::Base.extend Dragonfly::Model::Validations
end
