module Admin
  class PostsController < Admin::BaseController
    def index
      @posts = Post.all
    end

    def new
      @post = Post.new
    end

    def create
      @post = Post.new(post_params)

      if @post.save
        redirect_to admin_posts_path, alert: 'Post criado com sucesso'
      else
        flash[:alert] = 'Verifique os dados inseridos'
        render :new
      end
    end

    def edit
      @post = Post.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to admin_posts_path, alert: 'Post não encontrado'
    end

    def update
      @post = Post.find(params[:id])

      if @post.update(post_params)
        redirect_to admin_posts_path, alert: 'Post atualizado com sucesso'
      else
        flash[:alert] = 'Verifique os dados inseridos'
        render :edit
      end
    end

    def destroy
      @post = Post.find_by_id(params[:id])
      @post.destroy if @post.present?

      redirect_to admin_posts_path, alert: 'Post deletado com sucesso'
    end

    private

    def post_params
      params.require(:post).permit(:category_id, :title, :text, :image)
    end
  end
end