class PostsController < ApplicationController
  def index
    @posts = Post.recent.limit(5)

    respond_to do |format|
      format.html
      format.json { render json: @posts.as_json }
    end
  end

  def show
    @post = Post.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end
end