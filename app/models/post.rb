class Post < ActiveRecord::Base
  belongs_to :category, inverse_of: :posts
  has_many :comments

  validates :title, :text, :category, presence: true

  scope :recent, -> { order(created_at: :desc) }

  before_create :generate_slug

  dragonfly_accessor :image

  def excerpt
    text.truncate(50)
  end

  private

  def generate_slug
    self.slug = title.parameterize
  end
end
