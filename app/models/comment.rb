class Comment < ActiveRecord::Base
  belongs_to :post

  validates :author, :content, :post, presence: true
end
