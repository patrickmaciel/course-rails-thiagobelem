FactoryGirl.define do
  factory :comment do
    author { Faker::Name.name }
    content { Faker::Lorem.paragraph }
    post
  end
end
