require 'rails_helper'

RSpec.describe PostsController do
  describe 'GET index' do
    let!(:posts) { create_list :post, 6 }
    let(:recent_posts) { Post.recent.limit(5) }

    before { get :index }

    it 'return OK' do
      expect(response).to be_ok
    end

    it 'include last 5 posts ordered by created_at DESC' do
      expect(assigns(:posts).size).to eq(5)
      expect(assigns(:posts)).to eq(recent_posts)
    end

    it 'include posts titles on the markup' do
      recent_posts.each do |post|
        expect(response.body).to include(post.title)
      end
    end
  end

  describe 'GET show' do
    context 'with valid id' do
      let!(:post) { create :post }

      before { get :show, id: post.id }

      it 'return OK' do
        expect(response).to be_ok
      end

      it 'include post object' do
        expect(assigns(:post)).to eq(post)
      end

      it 'include post title on the markup' do
        expect(response.body).to include(post.title)
      end
    end

    context 'with invalid id' do
      before { get :show, id: "fulano" }

      it 'redirect to home' do
        expect(response).to redirect_to(root_path)
      end
    end
  end
end
