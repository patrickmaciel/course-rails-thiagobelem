require 'rails_helper'

RSpec.describe CommentsController do

  describe "POST #create" do
    let(:comment_post) { create :post }

    let(:params) do
      { post_id: comment_post.id,
        comment: {
          author: "Fulano",
          content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit"
        } }
    end

    def do_action
      post :create, params
    end

    it 'create a comment' do
      expect do
        do_action
      end.to change { comment_post.comments.count }.from(0).to(1)
    end

    it 'redirect to post' do
      do_action
      expect(response).to redirect_to(post_path(comment_post))
    end
  end

end
