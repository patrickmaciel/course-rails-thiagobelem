require 'rails_helper'

RSpec.describe Admin::PostsController do
  describe 'GET index' do
    let!(:posts) { create_list :post, 3 }

    before { get :index }

    it 'return OK' do
      expect(response).to be_ok
    end

    it 'include the posts' do
      expect(assigns(:posts)).to match_array(posts)
    end
  end

  describe 'GET new' do
    before { get :new }

    it 'return OK' do
      expect(response).to be_ok
    end

    it 'include the post' do
      expect(assigns(:post)).to be_a(Post)
      expect(assigns(:post)).to_not be_persisted
    end
  end

  describe "POST #create" do
    let(:category) { create :category }

    def do_action(params)
      post :create, post: params
    end

    let(:valid_params) do
      { category_id: category.id,
        title: "Meu post",
        text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit" }
    end

    context 'valid data' do
      it 'create a post' do
        expect { do_action(valid_params) }.to change { Post.count }.from(0).to(1)
      end

      it 'redirect to post' do
        do_action(valid_params)
        expect(response).to redirect_to(admin_posts_path)
      end
    end

    context 'invalid data' do
      let(:invalid_params) do
        valid_params.merge(title: '')
      end

      it 'does not create a post' do
        expect { do_action(invalid_params) }.to_not change { Post.count }
      end

      it 'render form' do
        do_action(invalid_params)
        expect(response).to render_template(:new)
      end
    end
  end

  describe 'GET edit' do
    let(:post) { create :post }

    context 'valid data' do
      before { get :edit, id: post.id }

      it 'return OK' do
        expect(response).to be_ok
      end

      it 'include the post' do
        expect(assigns(:post)).to eq(post)
      end
    end

    context 'invalid data' do
      before { get :edit, id: 0 }

      it 'redirect to posts index' do
        expect(response).to redirect_to(admin_posts_path)
      end
    end
  end

  describe "PUT #update" do
    let(:my_post) { create :post }

    def do_action(params)
      put :update, id: my_post.id, post: params
    end

    let(:valid_params) do
      { category_id: my_post.category.id,
        title: "Novo título",
        text: my_post.text }
    end

    context 'valid data' do
      it 'update the post' do
        do_action(valid_params)
        expect(my_post.reload.title).to eq('Novo título')
      end

      it 'redirect to index' do
        do_action(valid_params)
        expect(response).to redirect_to(admin_posts_path)
      end
    end

    context 'invalid data' do
      let(:invalid_params) do
        valid_params.merge(title: '')
      end

      it 'does not update the post' do
        do_action(invalid_params)
        expect(my_post.reload.title).to_not eq('Novo título')
      end

      it 'render form' do
        do_action(invalid_params)
        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'DELETE destroy' do
    let!(:post) { create :post }

    def do_action(post_id)
      delete :destroy, id: post_id
    end

    context 'valid post' do
      it 'delete the post' do
        expect { do_action(post.id) }.to change { Post.count }.by(-1)
      end

      it 'redirect to posts index' do
        do_action(post.id)
        expect(response).to redirect_to(admin_posts_path)
      end
    end

    context 'invalid post' do
      it 'do not delete the post' do
        expect { do_action(0) }.to_not change { Post.count }
      end

      it 'redirect to posts index' do
        do_action(0)
        expect(response).to redirect_to(admin_posts_path)
      end
    end
  end

end
