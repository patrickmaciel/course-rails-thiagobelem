require 'rails_helper'

RSpec.describe Post do
  describe 'relations' do
    it { should belong_to(:category) }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:text) }
    it { should validate_presence_of(:category) }
  end

  describe 'scopes' do
    describe '.recent' do
      let!(:post_a) { create :post, created_at: 1.hour.ago }
      let!(:post_b) { create :post, created_at: 1.minute.ago }
      let!(:post_c) { create :post, created_at: 10.minutes.ago }

      it 'return posts ordered by created_at desc' do
        expect(Post.recent).to eq([post_b, post_c, post_a])
      end
    end
  end

  describe '#excerpt' do
    let(:post) { create :post, text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa harum molestiae adipisci nemo facilis mollitia eligendi quaerat, quia tempora quae ea incidunt. Sed blanditiis doloribus porro voluptates, autem perspiciatis necessitatibus optio, a officia, quod libero maxime praesentium! Nemo, esse, deleniti.' }

    it 'return first 50 characters' do
      expect(post.excerpt).to eq('Lorem ipsum dolor sit amet, consectetur adipisi...')
    end
  end

  describe 'callbacks' do
    describe 'after create' do
      it 'generate slug based on title' do
        post = create :post, title: 'Olá mundo!'
        expect(post.slug).to eq('ola-mundo')
      end
    end
  end
end