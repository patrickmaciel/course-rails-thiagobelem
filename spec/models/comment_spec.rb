require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe 'relations' do
    it { should belong_to(:post) }
  end

  describe 'validations' do
    %i(author content post).each do |attr|
      it { should validate_presence_of(attr) }
    end
  end
end
